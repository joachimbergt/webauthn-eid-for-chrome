/**
 * WebAuthn-eID for Chrome (c) 2020 j_bergt
 * port of :  WebAuthn-eID for Firefox (c) 2020 kahlo@adesso
 */

var fidelio = function fidelio() {'use strict';
	console.log("eID.background.fidelio");
    var FIDELIO_URL = "https://id1.vx4.eu/fidelio/" + "eID2/?data=";
    function getOptions() {
    	chrome.storage.local.get("fidelio_url", function(result) {
    		FIDELIO_URL = (result.fidelio_url || "https://id1.vx4.eu/fidelio/") + "eID2/?data=";
    	    console.log("FIDELIO_URL = " + FIDELIO_URL);
    	});
    }
    chrome.storage.onChanged.addListener(getOptions);
    getOptions();

    function frameGet(serviceURL, cb) {
        console.log("eID.background.fidelio.frameGet : " + serviceURL);
        fetch(serviceURL)
			.then(s =>  { console.dir(s);return s.json(); })
			.then(sj => {
				console.log("eID.background.fidelio.frameGet.fetch");
				var clientFetch = fetch(sj.client, {redirect:"follow", mode:'no-cors'}).then(cf => {console.log("eID.background.fidelio.frameGet.fetch.clientFetch");console.dir(cf);}).catch(console.log).finally(null);
				var resultFetch = fetch(sj.result, {redirect:"follow"}).then(rf => {console.log("eID.background.fidelio.frameGet.fetch.resultFetch");return rf.arrayBuffer(); }).then(resultResponseArray => { cb && cb(new Uint8Array(resultResponseArray))}).catch(console.log).finally(null);
        	}).catch(console.log);
    }

    function json2str(obj) { try { return JSON.stringify(obj, null, ''); } catch (e) { return JSON.encode(obj); } }
    function str2json(str) { try { return JSON.parse(str, null, ''); } catch (e) { return JSON.decode(obj); } }
    function toHex(array) {
        try { return array.split('').map(function(char) {
        return ('0' + (char.charCodeAt(0) & 0xFF).toString(16)).slice(-2);
    }).join(''); } catch(e) { try { return array.reduce(function(memo, i) {
        return memo + ("0" + i.toString(16)).slice(-2); }, ''); } catch(e) { return toHex(new Uint8Array(array)); } } }
    function u82b64u(u8) {
        return encodeURIComponent(btoa(String.fromCharCode.apply(null, u8)).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '').trim());
    }
    function b64u2u8(encoded) { encoded = encoded.replace(/-/g, '+').replace(/_/g, '/');
    	while (encoded.length % 4) encoded += '='; return atob(encoded || '', 'base64'); }
    function s2u8(str) { return (new Uint8Array(str.length)).map(function(x, i) { return str.charCodeAt(i) & 0xFF; }); }
    function u82s(buf) { let str = ""; buf.map(function(x){ return str += String.fromCharCode(x) }); return str; }

    async function sha256(data) {
    	const hash = await crypto.subtle.digest("SHA-256", data).then(function(v) { return new Uint8Array(v); });
        return hash;
    };

    function callService(cmd, message, cb) {
        var cbor = new Uint8Array(CBOR.encode(message)), ctap = new Uint8Array(1 + cbor.length);
        ctap.set([cmd]);
        ctap.set(cbor, 1);
        var svcMsg = u82b64u(ctap);
        console.log(svcMsg);
        frameGet(FIDELIO_URL + svcMsg, function(data) {
            if(!data) { cb(data); return; }
            data = new Uint8Array(data, 0, data.length);
            var status = data[0], msg = CBOR.decode(data.buffer.slice(1));
            console.dir("STATUS: " + status + " MSG: " + msg);
            cb(msg['3'], msg['1'], status);
        });
    }

    var fidelioEN = function fidelioEN(appIdHash, clientDataHash, blackList, cb) {
        callService(1, {1 : appIdHash, 2 : clientDataHash, 5 : blackList}, cb); };
    var fidelioAU = function fidelioAU(appIdHash, clientDataHash, whiteList, cb) {
        callService(2, {1 : appIdHash, 2 : clientDataHash, 3 : whiteList}, cb); };

    function nccreate(options, rpId) {
        return new Promise(async function(resolve, reject) {
            // check for publicKey request, alg: -7, etc, running eID-Client, then lazy-load implementation?
            console.dir(options);

            if(!(options.publicKey.challenge instanceof Uint8Array)) {
                options.publicKey.challenge = Uint8Array.from(Object.values(options.publicKey.challenge));
            }

            console.log(json2str(options));
            var validKeys = 0;
            (options.publicKey.pubKeyCredParams || options.publicKey.parameters).forEach(function(item) {
                if (item.type === "public-key" && (item.alg === -7 || item.alg === "ES256")) {
                    validKeys = 1;
                }
            });

            if(validKeys) try {
                let appId = options.publicKey != null && options.publicKey.extensions != null ? options.publicKey.extensions.appid : null;
                if(appId != null) {
                    // check against asset list, smashes CORS errors with Google
                    // fetch(appId).then() ... function(err, data) { if(err !== null) { alert('Something went wrong: ' + err); } else {
                    //    console.log("asset list: " + data);
                    //}});
                } else {
                    appId = options.publicKey.rpId && rpId.domain.endsWith(options.publicKey.rpId) ? options.publicKey.rpId : rpId.domain;
                }

                let appIdHash = await sha256(s2u8(appId));
                let clientData = s2u8(json2str({"challenge": u82b64u(options.publicKey.challenge), "origin": rpId.origin, "type": "webauthn.create"}));
                let cdHash = await sha256(clientData);

                console.log("appID: " + toHex(appIdHash) + " clientData: " + toHex(clientData) + " clientDataHash: " + toHex(cdHash));

                var blackList = [];
                options.publicKey.excludeCredentials && options.publicKey.excludeCredentials.forEach(function(item) {
                    if (item.type === "public-key" && u82b64u(item.id).startsWith('8d4RA')) blackList.push(item.id);
                });

                console.log("appId-Raw: " + appId);
                console.log("rpId: " + rpId.domain);
                
                return fidelioEN(appIdHash, cdHash, blackList, function(regData, keyHandle, status) {
                    console.log(regData, keyHandle, status);
                    if(regData && status == 0) {
                        if (regData[0] != 0x05) {
                            log("Reserved byte wrong: " + regData[0]);
                            reject(null);
                            return;
                        }

                        let pubKey = regData.subarray(1, 66);
                        let keyHandle = regData.subarray(67, 67 + regData[66]);
                        let attestation = regData.subarray(67 + regData[66]);

                        console.log("public key : " + toHex(pubKey));
                        console.log("key handle : " + toHex(keyHandle));

                        if (attestation[0] = 0x30) {
                            let certLen = 0;
                            if (attestation[1] & 0x80 == 0) {
                              certLen = attestation[1] + 2;
                            } else if (attestation[1] == 0x81) {
                              certLen = attestation[2] + 3;
                            } else if (attestation[1] == 0x82) {
                              certLen = (attestation[2] << 8) + attestation[3] + 4;
                            }

                            var attCrt = attestation.subarray(0, certLen);
                            var attSig = attestation.subarray(certLen);
                        }

                        console.log("att-sig    : " + toHex(attSig));
                        console.log("att-crt    : " + toHex(attCrt));

                        let pkArray = [[1, 2], [3, -7], [-1, 1], [-2, pubKey.subarray(1, 33)], [-3, pubKey.subarray(33, 65)]];
                        let pubKeyEncoded = new Uint8Array(CBOR.encode(new Map(pkArray)));

                        let authData = new Uint8Array(55 + keyHandle.length + pubKeyEncoded.length);
                        authData.set(appIdHash, 0);

                        //authData.set([0x45], 32);             // attestation data included, user is verified, user is present
                        authData.set([0x41], 32);               // attestation data included, user is present

                        authData.set([0, 0, 0, 0], 33); // signature counter = 0
                        authData.set([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 37);     // AAGUID = 0

                        authData.set([keyHandle.length >> 8, keyHandle.length & 0xFF], 53);
                        authData.set(keyHandle, 55);

                        authData.set(pubKeyEncoded, 55 + keyHandle.length);

                        console.log("authData : " + toHex(authData));

                        let attObj = { "authData": authData, "fmt": "none", "attStmt": { } };

                        if(options.publicKey["attestation"] && (options.publicKey.attestation == "direct")) {
                            attObj = { "authData": authData, "fmt": "fido-u2f", "attStmt": { "x5c": [ attCrt ], "sig": attSig } };
                        } else { //indirect, none
                            //attObj = { "authData": authData, "fmt": "fido-u2f", "attStmt": { "x5c": [ attCrt ], "sig": attSig } };
                            // attestation object stays as "none"
                        }

                        console.log("attObj   : " + toHex(CBOR.encode(attObj)));

                        resolve({ "rawId": keyHandle, "id": u82b64u(keyHandle), "type" : "public-key", "response" : { "attestationObject" : new Uint8Array(CBOR.encode(attObj)), "clientDataJSON" : clientData } });
                        return;
                    }
                });
            } catch(e) { console.log(e); }
            });
    };

    function ncget(options, rpId) {
        return new Promise(async function(resolve, reject) {
            // check for publicKey request, alg: -7, etc, running eID-Client, then lazy-load implementation?
            console.dir(options);

            if(!(options.publicKey.challenge instanceof Uint8Array)) {
                options.publicKey.challenge = Uint8Array.from(Object.values(options.publicKey.challenge));
            }

            console.log(json2str(options));
            var whiteList = [];
            (options.publicKey.allowCredentials || options.publicKey.allowList).forEach(function(item) {
                if(!(item.id instanceof Uint8Array)) {
                    item.id = Uint8Array.from(Object.values(item.id));
                }
                if (item.type === "public-key" && u82b64u(item.id).startsWith('8d4RA')) whiteList.push(item.id);
            });

            console.dir(options);
            if(whiteList.length > 0) try {
                let appId = !document.domain.endsWith("twitter.com") && options.publicKey != null && options.publicKey.extensions != null ? options.publicKey.extensions.appid : null;
                if(appId != null) {
                    // check against asset list, smashes CORS errors with Google
                    //getJSON(appId, function(err, data) { if(err !== null) { alert('Something went wrong: ' + err); } else {
                    //    console.log("asset list: " + data);
                    //}});
                } else {
                    appId = options.publicKey.rpId && rpId.domain.endsWith(options.publicKey.rpId) ? options.publicKey.rpId : rpId.domain;
                }

                let appIdHash = await sha256(s2u8(appId));
                let clientData = s2u8(json2str({"challenge": u82b64u(options.publicKey.challenge), "origin": rpId.origin, "type": "webauthn.get"}));
                let cdHash = await sha256(clientData);

                console.log("appID: " + toHex(appIdHash) + " clientData: " + toHex(clientData) + " clientDataHash: " + toHex(cdHash));

                return fidelioAU(appId, cdHash, whiteList, function(signData, keyHandle, status) {
                    if(status == 0) {
                        let upFlag = signData.subarray(0, 1);
                        let counter = signData.subarray(1, 5);
                        let signature = signData.subarray(5);

                        console.log("signData   : " + toHex(signData));
                        console.log("userpresent: " + toHex(upFlag));
                        console.log("counter    : " + toHex(counter));
                        console.log("usr-sig    : " + toHex(signature));
                        console.log("key handle : " + toHex(keyHandle));

                        let authData = new Uint8Array(37);
                        authData.set(appIdHash, 0);
                        authData.set(upFlag, 32);       // attestation data not included, user is present
                        authData.set(counter, 33);      // signature counter = 0

                        let userHandle = new Uint8Array(0);

                        console.log("authData : " + toHex(authData));
                        resolve({"id": u82b64u(keyHandle), "rawId": keyHandle, "type": "public-key", "response": {
                            "authenticatorData": authData, "userHandle": userHandle, "signature": signature, "clientDataJSON": clientData } });
                        return;
                    }
                });
            } catch(e) { console.log(e); }
        });
    };

    async function u2freg(appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds, origin) {
        var challenge = null, blackList = [];
        registerRequests && registerRequests.forEach(function(request) {
            if (request.version === 'U2F_V2' && challenge == null) challenge = request.challenge;
        });

        if (challenge) {
            registeredKeys && registeredKeys.forEach(function(k) {
                if (k.version === 'U2F_V2' && k.keyHandle.startsWith('8d4RA')) blackList.push(k.keyHandle);
            });

            //appId = appId && appId.startsWith(document.location.origin) ? appId : document.location.origin;
            // instead fetch appId - which wouldn't work anyway?
            origin = origin == undefined ? document.location.origin : origin;
            console.log("U2F.register for " + origin + " as " + appId);
            let clientData = s2u8(json2str({"challenge": challenge, "origin": origin, "typ": "navigator.id.finishEnrollment"}));
            let cdHash = await sha256(clientData);

            return fidelioEN(appId, cdHash, blackList, function(response, credential) {
                callback({
                        "errorCode": 0, "version": 'U2F_V2', "clientData": u82b64u(clientData), "registrationData": u82b64u(response)
                });
            });
        }
    };

    async function u2fsig(appId, challenge, registeredKeys, callback, opt_timeoutSeconds, origin) {
        var whiteList = [];
        registeredKeys && registeredKeys.forEach(function(signReq) {
            if (signReq.version === 'U2F_V2' && signReq.keyHandle.startsWith('8d4RA')) whiteList.push(signReq.keyHandle);
        });

        if (whiteList && whiteList.length > 0) {
            //appId = appId && appId.startsWith(document.location.origin) ? appId : document.location.origin;
            console.log("U2F.register for " + origin + " as " + appId);
            console.log("U2F.sign for " + origin + " as " + appId);
            let clientData = s2u8(json2str({"challenge": challenge, "origin": origin, "typ": "navigator.id.getAssertion"}));
            let cdHash = await sha256(clientData);

            return fidelioAU(appId, cdHash, whiteList, function(response, credential) {
                callback({
                        "errorCode": 0, "version": "U2F_V2", "clientData": u82b64u(clientData), "keyHandle": u82b64u(credential), "signatureData": u82b64u(response)
                });
            });
        }

    };
    
	console.log("eID.background.fidelio : registering runtime.MessageListener");
    chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    	console.log("eID.background.fidelio.onMessage: " + JSON.stringify(sender) + " / " + JSON.stringify(msg));
    	
    	if(msg.call === "create") {
			nccreate(msg.options, msg.rpId)
				.then(cx => {
					console.log("eID.background.fidelio.onMessage.create.callback");
					console.dir(cx);
					sendResponse(cx);
					return;
				})
			return true;
    	} else if(msg.call === "get") {
			ncget(msg.options, msg.rpId)
				.then(cx => {
					console.log("eID.background.fidelio.onMessage.get.callback");
					console.dir(cx);
					sendResponse(cx);
					return;
				})
			return true;
    	} else if(msg.call === "u2freg") {
    		u2freg(msg.options[0], msg.options[1], msg.options[2], function(x) { sendResponse(x) }, msg.options[3], msg.rpId.origin); return true;
    	} else if(msg.call === "u2fsig") {
    		u2fsig(msg.options[0], msg.options[1], msg.options[2], function(x) { sendResponse(x) }, msg.options[3], msg.rpId.origin); return true;
    	} else { sendResponse("invalid call") }
    });

}();
console.log("eID.background");