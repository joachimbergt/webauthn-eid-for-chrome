/**
 * WebAuthn-eID for Chrome (c) 2020 j_bergt
 * port of :  WebAuthn-eID for Firefox (c) 2020 kahlo@adesso
 */
(function inject() { 'use strict';
	console.log("eID.content.inject");
	const is = document.createElement("script");
	is.type="text/javascript",
	is.src="chrome-extension://"+chrome.runtime.id+"/inject.js",
	document.documentElement.appendChild(is);
})();

(function relay() { 'use strict';

	var refreshUint8Array = (o) => {
		if (o instanceof Uint8Array) return o;
		return Uint8Array.from(Object.values(o));
	}
	const allowedMessages = {
		'eID.create' : (o) => {
			o.rawId = refreshUint8Array(o.rawId);
			var resp = o.response;
			if (resp) {
				resp.attestationObject = refreshUint8Array(resp.attestationObject);
				resp.clientDataJSON = refreshUint8Array(resp.clientDataJSON);
			}
			return o;} ,
		'eID.get': (o) => {
			o.rawId = refreshUint8Array(o.rawId);
			var resp = o.response;
			if (resp) {
				resp.authenticatorData = refreshUint8Array(resp.authenticatorData);
				resp.clientDataJSON = refreshUint8Array(resp.clientDataJSON);
				resp.signature = refreshUint8Array(resp.signature);
				resp.userHandle = refreshUint8Array(resp.userHandle);
			}
		return o},
		'eID.u2freg': (o) => {return o},
		'eID.u2fsign': (o) => {return o}
	};
	console.log("eID.content.relay : registering window.MessageListener");
	window.addEventListener('message', (evt) => {
		if (evt.source !== window) {
			return;
		}
		console.log("eID.content.relay.onMessage");
		if (evt.data.type && allowedMessages[evt.data.type]) {
			console.log("eID.content.relay.onMessage : dispatching to background-script");
			console.dir(evt.data.payload);
			chrome.runtime.sendMessage(evt.data.payload, (resp) => {
				console.log("eID.content.relay.onMessage.callback : sending response for " + evt.data.type);
				console.dir(resp);
				// rebuild response
				resp = allowedMessages[evt.data.type](resp);
				console.dir(resp);
				window.postMessage(
					{
						type: evt.data.type + ".response",
						payload: resp
					},
					window.location.origin
				);
			});
		}
	},false);}
)();
