/**
 * WebAuthn-eID for Chrome (c) 2020 j_bergt
 * port of :  WebAuthn-eID for Firefox (c) 2020 kahlo@adesso
 */
(function wrap() { 'use strict';

	var callbackFunctions = {}

	console.log("eID.inject.wrap : registering message-Event-Listener");
	window.addEventListener('message', (evt) => {
		console.log("eID.inject.wrap.onMessage");
		console.dir(evt);
		
		if (callbackFunctions[evt.data.type]) {
			console.log("eID.inject.wrap.onMessage : Relaying " + evt.data.type);
			console.dir(evt.data.payload);
			callbackFunctions[evt.data.type](evt.data.payload);
		}
	}, true);
	
	async function send(msg, resolve) {
		console.log("eID.inject.wrap.send");
		console.dir(msg);
		// register the response-message-callback!
		console.log("eID.inject.wrap.send : Storing callback for eID." + msg.call + ".response -> " + resolve);
		callbackFunctions["eID." + msg.call + ".response"] = resolve;
		console.log("eID.inject.wrap.send : Sending postMessage to content-script");
		window.postMessage(
			{
				type: "eID." + msg.call,
				payload: msg
			},
			window.location.origin
		);
	};
		
    let rpId = { origin: document.location.origin, domain: document.domain };
    		
    // WebAuthn
	if (navigator.credentials) {
		var nativeCredentials = {};
		var nativeCredentialsWrapped = {};
		var eIDCredentials = {};
		var allCredentials = [nativeCredentialsWrapped, eIDCredentials];
		var wrap = {};
		['create','get'].forEach(f => {
			nativeCredentials[f] = navigator.credentials[f];
			nativeCredentialsWrapped[f] = async (options) => {
					return new Promise(
					    // inner function used, to cature the "reject"-Function, when the USB-Prompt is canceled!
						async function(resolve, reject) {
							nativeCredentials[f].bind(navigator.credentials)(options)
								.then(resolve)
								.catch(c => {console.log("Native." + f + ".catch");console.dir(c);});
						}
					)
			}
			eIDCredentials[f] = async (options) => {
					return new Promise(
						async function(resolve, reject) {
							send({call: f, rpId: rpId, options: options}, resolve)
						}
			)};
			wrap[f] = async (options) => {return Promise.race(allCredentials.map(c => c[f](options)))}
		});
		Object.assign(navigator.credentials, wrap);
	} else {
		console.log("eID.inject.wrap : navigator.credentials not declared");
	}

    // U2F
	if (window.u2f) {
		console.log("eID.inject.wrap : Overriding window.u2f-functions");
		var u2fwrapper = async (appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds, call, protoCall) => {
			console.log("eID.inject.wrap.u2f : Patching call " + call);
			Promse.race(
				[
					new Promise(
						async function(resolve, reject) {
							send(
								{
									call: call,
									rpId: rpId,
									options: [appId, registerRequests, registeredKeys, opt_timeoutSeconds]
								},
								resolve
							)
						}
					),
					new Promise(
						async function(resolve, reject) {
							protoCall(appId, registerRequests, registeredKeys, resolve, opt_timeoutSeconds);
						}
					)
				]
			).then( (cb) => { callback(cb) });
		}
		window.u2f.register = (appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds)  => {
			return u2fwrapper(appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds, "u2freg", window.u2f.__proto__.register);
		};
		window.u2f.sign = (appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds)  => {
			return u2fwrapper(appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds, "u2fsig", window.u2f.__proto__.sign);
		};
	} else {
		console.log("eID.inject.wrap : window.u2f not declared");
	}
})();