function getOptions() {
	browser.storage.sync.get("fidelio_url").then(function(result) {
		document.querySelector("#fidelio_url").value = result.fidelio_url || "https://id1.vx4.eu/fidelio/";
	}, function(error) {
		console.log("Error: ${error}");
	});
}

document.addEventListener("DOMContentLoaded", getOptions);
document.querySelector("form").addEventListener("submit", function(e) {
	e.preventDefault();
	browser.storage.sync.set({
		fidelio_url : document.querySelector("#fidelio_url").value
	});
});
